#include "stm32f446xx.h" // Include the STM32F446RE header file
#include "stm32f446xx_gpio.h" // Include the GPIO driver header file

// Function to provide a delay
void delay(void) {
    for (uint32_t i = 0; i < 500000; ++i); // Adjust the loop count for desired delay
}

int main(void) {
    // Initialize GPIO handle structure
    GPIO_Handle_t gpioLED;
    gpioLED.pGPIOx = GPIOD; // Assuming the LED is connected to GPIOD
    gpioLED.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_12; // Assuming LED connected to Pin 12 of GPIOD
    gpioLED.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_OUT;
    gpioLED.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH;
    gpioLED.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_NO_PU_PD;
    gpioLED.GPIO_PinConfig.GPIO_PinOPType = GPIO_OUT_TYPE_PP;
    gpioLED.GPIO_PinConfig.GPIO_PinAltFunMode = GPIO_MODE_OUT;

    // Enable clock for GPIOD peripheral
    GPIO_PeriClockControl(GPIOD, ENABLE);

    // Initialize the GPIO pin
    GPIO_Init(&gpioLED);

    while (1) {
        // Toggle the LED
        GPIO_ToggleOutputPin(GPIOD, GPIO_PIN_NO_12); // Toggle Pin 12 of GPIOD

        // Introduce a delay
        delay();
    }

    return 0;
}
