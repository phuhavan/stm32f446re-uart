#include <stdint.h>

// Function prototypes of the functions in the Reset_Handler
void Reset_Handler(void);
void NMI_Handler(void) __attribute__((weak, alias("Default_Handler")));
void HardFault_Handler(void) __attribute__((weak, alias("Default_Handler")));
// Add other interrupt handlers as needed...

// Symbols defined in the linker script
extern uint32_t _etext;
extern uint32_t _sdata;
extern uint32_t _edata;
extern uint32_t _sbss;
extern uint32_t _ebss;

// Default handler for unused interrupt vectors
void Default_Handler(void) {
    while (1);
}

extern uint32_t _estack; // Declare _estack as an external symbol

// Cortex-M4 core interrupt vector table
uint32_t * vectors[] __attribute__((section(".isr_vector"))) = {
    // Core Level - CM4
    (uint32_t *)(&_estack),            // Initial Stack Pointer
    (uint32_t *)Reset_Handler,         // Reset Handler
    (uint32_t *)NMI_Handler,           // NMI Handler
    (uint32_t *)HardFault_Handler,     // Hard Fault Handler
    // Add other interrupt handlers as needed...
};

// Main application entry point
int main(void);

// Reset Handler - Entry point after microcontroller reset
void Reset_Handler(void) {
    // Copy data from Flash to RAM
    uint32_t *src = &_etext;
    uint32_t *dest = &_sdata;

    while (dest < &_edata) {
        *dest++ = *src++;
    }

    // Initialize data in bss section to zero
    dest = &_sbss;
    while (dest < &_ebss) {
        *dest++ = 0;
    }

    // Call the application entry point
    main();

    // If main() returns, enter an infinite loop
    while (1);
}
